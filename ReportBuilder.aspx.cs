﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public class Helper2 {

    public byte[] ReadFully(Stream input)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            input.CopyTo(ms);
            return ms.ToArray();
        }
    }

}
public partial class ReportBuilder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btSubmit_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(tbInputData.Text) && !string.IsNullOrEmpty(tbURL.Text))
        {
           // string sResponse = HttpPOST(tbURL.Text, tbInputData.Text);//
            string sResponse =  POST(tbURL.Text, tbInputData.Text);
            
            byte[] bytes = GetBytes(sResponse); //Convert.FromBase64String(sResponse);

           // byte[] bytes = Convert.FromBase64String(sResponse);

            System.IO.FileStream stream = new FileStream(@"D:\temp3\file.zip", FileMode.CreateNew);
            System.IO.BinaryWriter writer =
                new BinaryWriter(stream);
            writer.Write(bytes, 0, bytes.Length);
            writer.Close();

            //Response.WriteFile(sResponse);
            //Response.End();

        }
        //const string FILE_PATH = "C:\\foo.pdf";
        //const string DOWNLOADER_URI = "https://site.com/cgi-bin/somescript.pl?file=12345.pdf&type=application/pdf";

        //using (var writeStream = OpenWrite(FILE_PATH))
        //{
        //    var httpRequest = WebRequest.Create(DOWNLOADER_URI) as HttpWebRequest;
        //    var httpResponse = httpRequest.GetResponse();
        //    httpResponse.GetResponseStream().CopyTo(writeStream);
        //    writeStream.Close();
        //}
    }

    static byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }


    private static string POST(string Url, string Data)
    {
        System.Net.WebRequest req = System.Net.WebRequest.Create(Url);
        req.Method = "POST";
        req.Timeout = 100000;
        req.ContentType = "application/json";
        byte[] sentData = Encoding.GetEncoding(1251).GetBytes(Data);
        req.ContentLength = sentData.Length;
        System.IO.Stream sendStream = req.GetRequestStream();
        sendStream.Write(sentData, 0, sentData.Length);
        sendStream.Close();
        System.Net.WebResponse res = req.GetResponse();
        System.IO.Stream ReceiveStream = res.GetResponseStream();
        System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
        //Кодировка указывается в зависимости от кодировки ответа сервера
        Char[] read = new Char[256];
        Byte[] readByte;


        int count = sr.Read(read, 0, 256);
        string Out = String.Empty;

        while (count > 0)
        {
            String str = new String(read, 0, count);
            Out += str;
            count = sr.Read(read, 0, 256);
        }
        return Out;
    }

    private static string HttpPOST(string Url, string Data)
    {
        //StringBuilder postData = new StringBuilder();
        //postData.Append(HttpUtility.UrlEncode(String.Format("username={0}&", "")));
        //postData.Append(HttpUtility.UrlEncode(String.Format("password={0}&", "")));
        //postData.Append(HttpUtility.UrlEncode(String.Format("url_success={0}&", "")));
        //postData.Append(HttpUtility.UrlEncode(String.Format("url_failed={0}", "")));

        ASCIIEncoding ascii = new ASCIIEncoding();
        byte[] postBytes = ascii.GetBytes(Data.ToString());

        // set up request object
        HttpWebRequest request;
        try
        {
            request = (HttpWebRequest)HttpWebRequest.Create(Url);
        }
        catch (UriFormatException)
        {
            request = null;
        }
        if (request == null)
            throw new ApplicationException("Invalid URL: " + Url);

        request.Method = "POST";
        request.ContentType = "application/json";// "application/x-www-form-urlencoded";
        request.ContentLength = postBytes.Length;
        request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";

        // add post data to request
        Stream postStream = request.GetRequestStream();
        postStream.Write(postBytes, 0, postBytes.Length);

            //
                //if (postStream != null)
                //{
                //    SetLinkPDF(postStream);
                //}

        //using (postStream = File.Create(filename))
        //{
        //    CopyStream(input, file);
        //}

       SaveStreamToFile("@D:\temp3\file.pdf", postStream);

        Helper2 helper = new Helper2();
       // Byte[] ttt = helper.ReadFully(postStream);

        //SetLinkPDF(helper.ReadFully(postStream), "qqq");
        

        postStream.Flush();
        postStream.Close();

        return "";
    }


    public static void SaveStreamToFile(string fileFullPath, Stream stream)
    {
        if (stream.Length == 0) return;

        // Create a FileStream object to write a stream to a file
        using (FileStream fileStream = System.IO.File.Create(fileFullPath, (int)stream.Length))
        {
            // Fill the bytes[] array with the stream data
            byte[] bytesInStream = new byte[stream.Length];
            stream.Read(bytesInStream, 0, (int)bytesInStream.Length);

            // Use FileStream object to write to the specified file
            fileStream.Write(bytesInStream, 0, bytesInStream.Length);
        }
    }

    //public Byte[] ReadFully(Stream input)
    //{
    //    Byte[] buffer = new byte[16 * 1024];
    //    using (MemoryStream ms = new MemoryStream())
    //    {
    //        int read;
    //        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
    //        {
    //            ms.Write(buffer, 0, read);
    //        }
    //        return ms.ToArray();
    //    }
    //}

    //public static byte[] ReadFully(Stream input)
    //{
    //    using (MemoryStream ms = new MemoryStream())
    //    {
    //        input.CopyTo(ms);
    //        return ms.ToArray();
    //    }
    //}

    private void SetLinkPDF(Byte[] ByteArray, string PDFName)
    {
        Response.Clear();

        //Send the file to the output stream

        Response.Buffer = true;

        //Try and ensure the browser always opens the file and doesn’t just prompt to “open/save”.

        Response.AddHeader("Content-Length", ByteArray.Length.ToString());

        Response.AddHeader("Content-Disposition", "inline; filename=" + PDFName);

        Response.AddHeader("Expires","0");

        Response.AddHeader("Pragma","cache");

        Response.AddHeader("Cache-Control","private");

        //Set the output stream to the correct content type (PDF).

        Response.ContentType = "application/pdf";

        //Output the file

        Response.BinaryWrite(ByteArray);

        //Flushing the Response to display the serialized data

        //to the client browser.

        Response.Flush();

        try{Response.End();}

        catch{}   
    }


}