﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" ValidateRequest="false" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="Translation.aspx.cs" Inherits="Translation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
     <asp:Button ID="btnSignOut" runat="server" Text="Sign Out" OnClick="btnSignOut_Click" />
    <asp:ObjectDataSource ID="odsTransalation" runat="server"
        SelectMethod="SelectAllTransalation" UpdateMethod="UpdateTranslationLabels"
        TypeName="Facade.BusinessLogic">
        <SelectParameters>
            <asp:ControlParameter ControlID="tbSearch" PropertyName="Text" Type="String"
                Name="searchparam" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="SystemTransiD" />
            <asp:Parameter Name="ModuleId" DefaultValue="1" />
            <asp:Parameter Name="ScreenName" />
            <asp:Parameter Name="PosistionOnScreen" />
            <asp:Parameter Name="EngLabelName" />
            <asp:Parameter Name="IsEngLabelActive" Type="Boolean" />
            <asp:Parameter Name="EspLabelName" />
            <asp:Parameter Name="IsEspLabelActive" Type="Boolean" />

            <asp:Parameter Name="HebLabelName" />
            <asp:Parameter Name="IsHebLabelActive" Type="Boolean" />

        </UpdateParameters>
    </asp:ObjectDataSource>
    <br />
    Enter text to search:
    <asp:TextBox ID="tbSearch" runat="server"></asp:TextBox><asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
    &nbsp;&nbsp;
    <asp:Button ID="bnCleart" runat="server" Text="Clear" OnClick="bnCleart_Click" />
    &nbsp;&nbsp;&nbsp;&nbsp; / &nbsp;
    <asp:Button ID="btnAddNew" runat="server" Text="Add New" OnClick="btnAddNew_Click" />
    <br />
    <br />
    <asp:ObjectDataSource ID="odsTransalationItem" runat="server"
        InsertMethod="InsertTranslationLabel"
        TypeName="Facade.BusinessLogic">
        <InsertParameters>
            <asp:Parameter Name="ModuleId" DefaultValue="1" />
            <asp:Parameter Name="ScreenName"/>
            <asp:Parameter Name="PosistionOnScreen"/>
            <asp:Parameter Name="LabelName" />
            <asp:Parameter Name="EngLabelName"/>
            <asp:Parameter Name="IsEngLabelActive" Type="Boolean" DefaultValue="False"/>
            <asp:Parameter Name="EspLabelName"/>
            <asp:Parameter Name="IsEspLabelActive" Type="Boolean" DefaultValue="False"/>

            <asp:Parameter Name="HebLabelName"/>
            <asp:Parameter Name="IsHebLabelActive" Type="Boolean" DefaultValue="False"/>

        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:DetailsView ID="dvTransalation" AutoGenerateRows="False" DefaultMode="Insert"
        DataSourceID="odsTransalationItem" runat="server" Visible="False" AutoGenerateInsertButton="true">
        <Fields>
            <asp:BoundField DataField="ScreenName" HeaderText="ScreenName" SortExpression="ScreenName" />
            <asp:BoundField DataField="PosistionOnScreen" HeaderText="PosistionOnScreen" SortExpression="PosistionOnScreen" />

            <asp:BoundField DataField="LabelName" HeaderText="LabelName" SortExpression="LabelName" />

            <asp:BoundField DataField="EngLabelName" HeaderText="EngLabelName" SortExpression="EngLabelName" />
            <asp:TemplateField HeaderText="IsEngLabelActive">
                <InsertItemTemplate>
                    <asp:CheckBox ID="chInsertIsEngLabelActive" runat="server" Checked='<%# Bind("IsEngLabelActive") %>' />
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="EspLabelName" HeaderText="EspLabelName" SortExpression="EspLabelName" />
            <asp:TemplateField HeaderText="IsEspLabelActive">
                <InsertItemTemplate>
                    <asp:CheckBox ID="chInsertIsEspLabelActive" runat="server" Checked='<%# Bind("IsEspLabelActive") %>' />
                </InsertItemTemplate>
            </asp:TemplateField>

            <asp:BoundField DataField="HebLabelName" HeaderText="HebLabelName" SortExpression="HebLabelName" />
            <asp:TemplateField HeaderText="IsHebLabelActive">
                <InsertItemTemplate>
                    <asp:CheckBox ID="chIsHebLabelActive" runat="server" Checked='<%# Bind("IsHebLabelActive") %>' />
                </InsertItemTemplate>
            </asp:TemplateField>


        </Fields>
    </asp:DetailsView>
    <br />
    <br />
    <asp:GridView ID="gvTransalation" AllowSorting="true" AllowPaging="true" runat="server"
        DataSourceID="odsTransalation" AutoGenerateEditButton="true" DataKeyNames="SystemTransiD"
        AutoGenerateColumns="False" PageSize="50">
        <Columns>
            <asp:BoundField ReadOnly="true" HeaderText="ID" DataField="SystemTransiD" SortExpression="SystemTransiD" />
            <asp:BoundField HeaderText="Screen Name" DataField="ScreenName" SortExpression="ScreenName" />
            <asp:BoundField HeaderText="Posistion on screen" DataField="PosistionOnScreen" SortExpression="PosistionOnScreen" />
            <asp:TemplateField HeaderText="Label Name">
                <ItemTemplate>
                    <asp:Label runat="server" ID="cbItemLabelName" Text='<%# Eval("LabelName") %>' Enabled="false" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="English Label Name" DataField="EngLabelName" SortExpression="EngLabelName" />
            <asp:TemplateField HeaderText="Is English Active">
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="cbItemIsEngLabelActive" Checked='<%# Eval("IsEngLabelActive") %>' Enabled="false" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="cbEditIsEngLabelActive" Checked='<%# Bind("IsEngLabelActive") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Spanish Label Name" DataField="EspLabelName" SortExpression="EspLabelName" />
            <asp:TemplateField HeaderText="Is Spanish Active">
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="cbItemIsEspLabelActive" Checked='<%# Eval("IsEspLabelActive") %>' Enabled="false" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="cbEditIsEspLabelActive" Checked='<%# Bind("IsEspLabelActive") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:BoundField HeaderText="Hebrew Label Name" DataField="HebLabelName" SortExpression="HebLabelName" />

            <asp:TemplateField HeaderText="Is Hebrew Active">
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="cbItemIsHebLabelActive" Checked='<%# Eval("IsHebLabelActive") %>' Enabled="false" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="cbEditIsHebLabelActive" Checked='<%# Bind("IsHebLabelActive") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>


        </Columns>
    </asp:GridView>
</asp:Content>

