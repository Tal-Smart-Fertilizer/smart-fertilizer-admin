﻿<%@ Page Title="" Language="C#" ValidateRequest="false" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReportBuilder.aspx.cs" Inherits="ReportBuilder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <br />
    <asp:Button ID="btSubmit" runat="server" Text="Test Report" OnClick="btSubmit_Click" />
    <br />
    <br />
    <div>
        Please input URL:
        <br />
        <asp:TextBox ID="tbURL" Style="width: 600px;" runat="server"></asp:TextBox>
        <br />
        Please input JSON POST data:
        <br />
        <asp:TextBox ID="tbInputData" TextMode="MultiLine" Style="width: 600px; height: 450px;" runat="server"></asp:TextBox>
    </div>
    <div></div>
</asp:Content>

