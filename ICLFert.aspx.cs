﻿using Smart.DataManager;
using Smart.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ICLFert : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        DataAccess da = new DataAccess();
        DataTable dt = da.SelectICLFert();


        int userId = 18;
        Fertilizer oFert;

        foreach (DataRow dataRow in dt.Rows)
        {
            try
            {
                oFert = new Fertilizer();

                oFert.IsSystemFert = "1";
                

                oFert.Name = dataRow["שם"].ToString() + "_"+ dataRow["הרכב"].ToString(); ;
                oFert.Manufacturer = "ICL";
                //
                oFert.Price = string.Empty;
                //oFert.Status = 1; //???
                oFert.State = 2;
               // oFert.SpecificWeight = 1.1;

                oFert.SpecificWeight = double.Parse(dataRow[4].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                oFert.PH = double.Parse( dataRow["pH"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                oFert.MinimumTemp = dataRow[7].ToString();

                

               // oFert.Nh2_Form = "22";

                //Solubility oSolubility = new Solubility();
                //oSolubility.Sol_0C = 1.1;
                //oSolubility.Sol_5C = 1.1;
                //oSolubility.Sol_10C = 1.1;
                //oSolubility.Sol_15C = 1.1;
                //oSolubility.Sol_20C = 1.1;
                //oSolubility.Sol_25C = 1.1;
                //oSolubility.Sol_30C = 1.1;
                ////
                //oFert.Solubility = oSolubility;
                //
                ElementsConcentrations ElConcentrations = new ElementsConcentrations();
               // ElConcentrations.B_Val = 1.1;
              //  ElConcentrations.Ca_Val = 1.1;
                try
                {
                    ElConcentrations.CL_Val = double.Parse(dataRow[16].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }
              //  ElConcentrations.CO3_Val = 1.1;
                try
                {
                    ElConcentrations.Cu_Val = double.Parse(dataRow[20].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }
             //   ElConcentrations.F_Val = 1.1;
                try
                {
                    ElConcentrations.Fe_Val = double.Parse(dataRow[17].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }
            //    ElConcentrations.HCO3_Val = 1.1;
                try
                {
                    ElConcentrations.K_Val = double.Parse(dataRow[14].ToString(), System.Globalization.CultureInfo.InvariantCulture); //K2O
                }
                catch { }

                try { ElConcentrations.Mg_Val = double.Parse(dataRow[15].ToString(), System.Globalization.CultureInfo.InvariantCulture); }
                catch { }
                
                ElConcentrations.MicroElementsChelated = 1; //by default

                try
                {
                    ElConcentrations.Mn_Val = double.Parse(dataRow[18].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }
                try
                {
                    ElConcentrations.Mo_Val = double.Parse(dataRow[21].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }
                try
                {
                    ElConcentrations.N_Val = double.Parse(dataRow[9].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }

            //    ElConcentrations.Na_Val = 1.1;

                try
                {
                    ElConcentrations.NH2_Val = double.Parse(dataRow[12].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }

                try
                {
                    ElConcentrations.NH4_Val = double.Parse(dataRow[11].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }
                try
                {
                    ElConcentrations.NO3_Val = double.Parse(dataRow[10].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }

                try
                {
                    ElConcentrations.P_Val = double.Parse(dataRow[13].ToString(), System.Globalization.CultureInfo.InvariantCulture); //P2O5
                }
                catch { }

            //    ElConcentrations.S_Val = 1.1;
                try
                {
                    ElConcentrations.Zn_Val = double.Parse(dataRow[19].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }
                //
                oFert.ElementsConcentrations = ElConcentrations;
                oFert.K_Form = "K20";
                oFert.P_Form = "P2O5";

                oFert.Nh2_Form = "N-NH2";
                oFert.Nh4_form = "N-NH4";
                oFert.No3_From = "N-NO3";

                //
                if (oFert.Price == string.Empty)
                    oFert.Price = "0";
                if (oFert.State == 4)
                    oFert.SpecificWeight *= 1000;
                int fertId = ManageFertilizer.addFertilizer(userId, oFert);

            }
            catch (Exception ex)
            {
            }
        }

    }
}