﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

public partial class TestCrops : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //string str = "aRTUR rOTENBERG";

        //string str2 = Helper.CapitalizeWords(str);

        Facade.BusinessLogic oLg = new Facade.BusinessLogic();
        DataTable dt = oLg.SelectCropData2();

        //GridView1.DataSource = dt;
        //GridView1.DataBind();

        int iCropId = -1;
        string sRusCropName = "";

        foreach (DataRow dataRow in dt.Rows)
        {
            iCropId = Int32.Parse(dataRow["EnityType"].ToString());
            sRusCropName = dataRow["Rus"].ToString();
            bool b = oLg.UpdateCropRus(iCropId, sRusCropName);
        }

        //foreach (DataRow dataRow in dt.Rows)
        //{
            //iCropId = Int32.Parse(dataRow["CropId"].ToString());
            //sEspCropName = dataRow["Esp"].ToString();
            // bool b = oLg.UpdateCropEsp(iCropId, sEspCropName);

            //var doc = new System.Xml.XmlDocument();

            //XDocument.Parse(dataRow["CropId"].ToString());
            //string strXml = dataRow["DataXml"].ToString();
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.LoadXml(strXml);

            //doc.Load(strXml);

            //var root = xmlDoc.FirstChild;

            //foreach (System.Xml.XmlNode child in xmlDoc.ChildNodes)
            //{
            //    //if (child.Attributes["Name"].ToString() != "Id" || 
            //    //    child.Attributes["Name"].ToString() != "Name" ||
            //    //    child.Attributes["Name"].ToString() != "StageName")

            //    //    child.Attributes.Remove(child.Attributes["Name"]);
            //    XmlAttribute attribute = null;

            //    foreach (XmlAttribute attri in child.Attributes)
            //    {
            //        if (attri.Name != "Id" || attri.Name != "Name" || attri.Name != "StageName")
            //        {
            //            attribute = attri;

            //            //child.RemoveAll();
            //            child.Attributes.RemoveNamedItem(attri.Name);
            //            break;
            //        }
            //    }
            //    // if (attribute != null)
            //    //child.ParentNode.RemoveChild(attribute);
            //    //child.ParentNode.Attributes.Remove(attribute); 
            //}

            //var root = xmlDoc.FirstChild;

        //}


    }




    private XmlDocument removeAttributesbyName(string[] ids, string strXml)
    {
        XmlDocument doc = new XmlDocument();
        //doc.Load(path);
        doc.LoadXml(strXml);
        XmlNodeList xnlNodes = doc.GetElementsByTagName("*");
        foreach (XmlElement el in xnlNodes)
        {
            for (int i = 0; i <= ids.Length - 1; i++)
            {
                if (el.HasAttribute(ids[i]))
                {
                    el.RemoveAttribute(ids[i]);
                }
                if (el.HasChildNodes)
                {
                    foreach (XmlNode child in el.ChildNodes)
                    {
                        if (child is XmlElement && (child as XmlElement).HasAttribute(ids[i]))
                        {
                            (child as XmlElement).RemoveAttribute(ids[i]);
                        }
                    }
                }
            }
        }
       // doc.Save("D:\\try2.xml");
        return doc;
    }


    protected void Button2_Click(object sender, EventArgs e)
    {
        Facade.BusinessLogic oLg = new Facade.BusinessLogic();
        DataTable dt = oLg.SelectCropData();
        string[] items = { "Status", 
                             "Type", 
                             "ImageId", 
                             "ImageURL",
                         "Width",
                         "Hight",
                         "PlantsPerHa",
                         "PlantsPerHaUnit",
                         "FertApplicationMethod",
                         "UnitId",
                         "IsActive",
                         "P_Cbo",
                         "K_Cbo",
                         "Ca_Cbo",
                         "Mg_Cbo",
                         "S_Cbo",
                         "N_Val",
                         "NO3_Val",
                         "NH2_Val",
                         "NH4_Val",
                         "P_Val",
                         "K_Val",
                         "Ca_Val",
                         "Mg_Val",
                         "S_Val",
                         "B_Val",
                         "Fe_Val",
                         "Mn_Val",
                         "Zn_Val",
                         "Cu_Val",
                         "Mo_Val",
                         "Na_Val",
                         "HCO3_Val",
                         "CO3_Val",
                         "CL_Val",
                         "F_Val",
                         "ApplicationInterval",
                         "Duration"};

        string strXml = "";
        //DataRow dr = dt.Select().FirstOrDefault();

        foreach (DataRow dr in dt.Rows)
        {
            strXml = dr["DataXml"].ToString();
            XmlDocument doc2 = removeAttributesbyName(items, strXml);
            doc2.Save("D:\\crop\\Crop" + dr["CropId"].ToString() + ".xml");
        }
     
        

    

        //int EntityId = Int32.Parse(dr["CropId"].ToString());
        //int EnityType = 2;  
        //int LanguageId = 1;

        //oLg.InsertEntityTranslation(EntityId, EnityType, doc2.InnerXml.ToString(), LanguageId);

       //doc2.Save("D:\\try2.xml");
    }


    protected void Button3_Click(object sender, EventArgs e)
    {
        string folderPath = "D:\\SMART\\Crops\\crop_rus\\crop_rus";
        Facade.BusinessLogic oLg = new Facade.BusinessLogic();
        string resultString;
        foreach (string file in Directory.EnumerateFiles(folderPath, "*.xml"))
        {
            string contents = File.ReadAllText(file);

            resultString = Regex.Match(file, @"\d+").Value;

            oLg.InsertEntityTranslation(1, Int32.Parse(resultString), contents, 8);
        }

    }

}