﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DbServer
/// </summary>
/// 
public class DbServer
{
    //public string DBName;
    string serverName;
    int serverId;
    string dataSource;
    string userId;
    string pass;

	public DbServer()
	{
	}
    public string ServerName
    {
        get
        {
            return this.serverName;
        }
        set
        {
            this.serverName = value;
        }
    }
    public int ServerId
    {
        get
        {
            return this.serverId;
        }
        set
        {
            this.serverId = value;
        }
    }
    public string DataSource
    {
        get
        {
            return this.dataSource;
        }
        set
        {
            this.dataSource = value;
        }
    }
    public string UserId
    {
        get
        {
            return this.userId;
        }
        set
        {
            this.userId = value;
        }
    }
    public string Pass
    {
        get
        {
            return this.pass;
        }
        set
        {
            this.pass = value;
        }
    }
}

public class ListOfServers
{
    public List<DbServer> stages = new List<DbServer>();

    public ListOfServers()
	{
        DbServer s1 = new DbServer();
        s1.ServerName = "Azure Production";
        s1.ServerId = 1;
        s1.DataSource = "191.237.44.51,1433\\SMART-FERT";
        s1.UserId = "sa2";
        s1.Pass = "S@0g1nSm@rt!";

        DbServer s2 = new DbServer();
        s2.ServerName = "Test/Dev Server";
        s2.ServerId = 2;
        s2.DataSource = "118.139.160.80\\SmartDev1";
        s2.UserId = "sa";
        s2.Pass = "Abcd1234";

        stages.Add(s1);
        stages.Add(s2);
	}
}