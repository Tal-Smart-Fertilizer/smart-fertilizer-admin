﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for EmailManagement
/// </summary>
public class EmailManagement
{
    #region members
    private string _username;
    private string _password;
    private string _smtpServer;
    private string _from;
    private int _timeOut;
    private bool _enableSsl;
    private int _port;
    #endregion

    #region Properties
    public string Username
    {
        get
        {
            return _username;
        }
        set
        {
            _username = value;
        }
    }
    public string Password
    {
        get
        {
            return _password;
        }
        set
        {
            _password = value;
        }
    }
    public string SmtpServer
    {
        get
        {
            return _smtpServer;
        }
        set
        {
            _smtpServer = value;
        }
    }
    public string From
    {
        get
        {
            return _from;
        }
        set
        {
            _from = value;
        }
    }
    public int Timeout
    {
        get
        {
            return _timeOut;
        }
        set
        {
            _timeOut = value;
        }
    }
    public bool EnableSsl
    {
        get
        {
            return _enableSsl;
        }
        set
        {
            _enableSsl = value;
        }
    }
    public int Port
    {
        get
        {
            return _port;
        }
        set
        {
            _port = value;
        }
    }
    #endregion

    public EmailManagement()
    {
    }

    public void SendMail(string recipient, string subject, string body, string[] attachments)
    {
        SmtpClient smtpClient = new SmtpClient();
        NetworkCredential basicCredential = new NetworkCredential(Username, Password, SmtpServer);
        MailMessage message = new MailMessage();
        MailAddress fromAddress = new MailAddress(From);

        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

        smtpClient.Host = SmtpServer;
        smtpClient.UseDefaultCredentials = false;
        smtpClient.Credentials = basicCredential;
        smtpClient.Timeout = Timeout;
        smtpClient.EnableSsl = EnableSsl;
        smtpClient.Port = Port;

        message.BodyEncoding = Encoding.UTF8;
        message.SubjectEncoding = Encoding.UTF8;

        message.From = fromAddress;
        message.IsBodyHtml = true;
        message.Subject = subject;

        AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, new ContentType("text/plain; charset=UTF-8"));
        // AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

        message.AlternateViews.Add(htmlView);

        //message.Body = body.ToString(); //body.Replace("\r\n", "<br>");
        message.To.Add(recipient);

        if (attachments != null)
        {
            foreach (string attachment in attachments)
            {
                message.Attachments.Add(new Attachment(attachment));
            }
        }
        smtpClient.Send(message);
    }

}