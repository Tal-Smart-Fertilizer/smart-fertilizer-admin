﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BusinessLogic
/// </summary>
/// 
namespace Facade
{
    public class BusinessLogic
    {
        #region members
        protected DataAccess m_oDataAccess;
        #endregion

        public BusinessLogic()
        {
            m_oDataAccess = new DataAccess();
        }

        public DataTable SelectAdminEmail(int EmailId)
        {
            return m_oDataAccess.SelectAdminEmail(EmailId);
        }

        public DataTable SelectAllAdminEmail()
        {
            return m_oDataAccess.SelectAllAdminEmail();
        }

        public int InsertAdminEmail(string Address, string Password, string Host, int Port, int Timeout, bool EnableSsl)
        {
            return m_oDataAccess.InsertAdminEmail(Address, Password, Host, Port, Timeout, EnableSsl);
        }

        public bool UpdateAdminEmail(int EmailId, string Address, string Password, string Host, int Port, int Timeout, bool EnableSsl)
        {
            return m_oDataAccess.UpdateAdminEmail(EmailId, Address, Password, Host, Port, Timeout, EnableSsl);
        }

        public bool DeleteAdminEmail(int EmailId)
        {
            return m_oDataAccess.DeleteAdminEmail(EmailId);
        }

        public DataTable SelectAdminMessage(int MessageId)
        {
            return m_oDataAccess.SelectAdminMessage(MessageId);
        }

        public DataTable SelectAllAdminMessage()
        {
            return m_oDataAccess.SelectAllAdminMessage();
        }

        public int InsertAdminMessage(string subject, string body)
        {
            return m_oDataAccess.InsertAdminMessage(subject, body);
        }

        public bool UpdateAdminMessage(int messageId, string subject, string body)
        {
            return m_oDataAccess.UpdateAdminMessage(messageId, subject, body);
        }

        public bool DeleteAdminMessage(int messageId)
        {
            return m_oDataAccess.DeleteAdminMessage(messageId);
        }


        public DataTable SelectCropData()
        {
            return m_oDataAccess.SelectCropData();
        }

        public DataTable SelectCropData2()
        {
            return m_oDataAccess.SelectCropData2();
        }

        public bool UpdateCropRus(int CropId, string RusCropName)
        {
            return m_oDataAccess.UpdateCropRus(CropId, RusCropName);
        }

        public bool UpdateCropEsp(int CropId, string EspCropName)
        {
            return m_oDataAccess.UpdateCropEsp(CropId, EspCropName);
        }

        public int InsertEntityTranslation(int EntityId, int EnityType, string EntityData, int LanguageId)
        {
            return m_oDataAccess.InsertEntityTranslation(EntityId, EnityType, EntityData, LanguageId);
        }

        public DataTable SelectAllTransalation()
        {
            return m_oDataAccess.SelectAllTransalation();
        }

        public DataTable SelectAllTransalation(string searchparam)
        {
            DataTable dt = null;
            if (!string.IsNullOrEmpty(searchparam))
            {
              dt =  m_oDataAccess.SearchTransalation(searchparam);
            }
            else { 
                dt = m_oDataAccess.SelectAllTransalation();
            }
            return dt;
        }

        public DataTable SearchTransalation(string searchparam)
        {
            return m_oDataAccess.SearchTransalation(searchparam);
        }

        public bool UpdateTranslationLabels(int ModuleId
                                            ,string ScreenName
                                            ,string PosistionOnScreen
                                            ,string EngLabelName
                                            , bool IsEngLabelActive
                                            ,string EspLabelName
                                            ,bool IsEspLabelActive
                                            ,int SystemTransiD
                                            //,string FraLabelName
                                            //,bool IsFraLabelActive
                                            ,string HebLabelName
                                            ,bool IsHebLabelActive
                                            //,string GrcLabelName
                                            //,bool IsGrcLabelActive
                                            //,string ItaLabelName
                                            //,bool IsItaLabelActive
                                            //,string PorLabelName
                                            //,bool IsPorLabelActive
                                            //,string PolLabelName
                                            //,bool IsPolLabelActive
                                            //,string RusLabelName
                                            //,bool IsRusLabelActive
            )
        {
            return m_oDataAccess.UpdateTranslationLabels( SystemTransiD
                                                        ,ModuleId
                                                        ,ScreenName
                                                        ,PosistionOnScreen
                                                        ,EngLabelName
                                                        ,IsEngLabelActive
                                                        ,EspLabelName
                                                        ,IsEspLabelActive
                                                        //,FraLabelName
                                                        //,IsFraLabelActive
                                                        ,HebLabelName
                                                        ,IsHebLabelActive
                                                        //,GrcLabelName
                                                        //,IsGrcLabelActive
                                                        //,ItaLabelName
                                                        //,IsItaLabelActive
                                                        //,PorLabelName
                                                        //,IsPorLabelActive
                                                        //,PolLabelName
                                                        //,IsPolLabelActive
                                                        //,RusLabelName
                                                        //,IsRusLabelActive
                                                        );
        }

        //public int InsertTranslationLabel(int ModuleId
        //                                 ,string ScreenName
        //                                 ,string PosistionOnScreen
        //                                 ,string EngLabelName
        //                                 ,bool IsEngLabelActive
        //                                 ,string EspLabelName
        //                                 ,bool IsEspLabelActive)
        //{
        //    return m_oDataAccess.InsertTranslationLabel( ModuleId
        //                                                 ,ScreenName
        //                                                 ,PosistionOnScreen
        //                                                 ,EngLabelName
        //                                                 ,IsEngLabelActive
        //                                                 ,EspLabelName
        //                                                 ,IsEspLabelActive);
        //}


        public void InsertTranslationLabel(int ModuleId
                                        , string ScreenName
                                        , string PosistionOnScreen
                                        , string LabelName
                                        , string EngLabelName
                                        , bool IsEngLabelActive
                                        , string EspLabelName
                                        , bool IsEspLabelActive
                                                        , string HebLabelName
                                            , bool IsHebLabelActive)
        {
            m_oDataAccess.InsertTranslationLabel(ModuleId
                                                         , ScreenName
                                                         , PosistionOnScreen
                                                         , LabelName
                                                         , EngLabelName
                                                         , IsEngLabelActive
                                                         , EspLabelName
                                                         , IsEspLabelActive
                                                         , HebLabelName
                                                        , IsHebLabelActive);
        }

        public DataTable GetDBList()
        {
            return m_oDataAccess.GetDBList();
        }

    }
}