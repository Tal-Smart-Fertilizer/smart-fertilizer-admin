﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;


/// <summary>
/// Summary description for DataAccess
/// </summary>
public class DataAccess
{
    #region Members
    private string m_sConnectionString = string.Empty;
    private string m_sConnectionStringProd = string.Empty;
    #endregion

    #region Constructor
    public DataAccess()
    {
        //m_sConnectionString = "Persist Security Info=False;Initial Catalog=SmartDB;User Id=sa;Password=S@L0g1n;Data Source=SMART-FERT\\SQLEXPRESS";

        //m_sConnectionString = "Persist Security Info=False;Initial Catalog=SmartDB;User Id=sa;Password=S@L0g1n;Data Source=137.117.35.180\\SQLEXPRESS";
        //m_sConnectionString = "Persist Security Info=False;Initial Catalog=SmartDB;User Id=sa;Password=Abcd1234;Data Source=118.139.160.80\\SmartDev1";
        //m_sConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();
        //m_sConnectionStringProd = "Persist Security Info=False;Initial Catalog=SMARTDB_ICL;User Id=sa2;Password=S@0g1nSm@rt!;Data Source=191.237.44.51,1433\\SMART-FERT";
        m_sConnectionString = "Persist Security Info=False;Initial Catalog=SmartDB_Compo;User Id=sa2;Password=S@0g1nSm@rt!;Data Source=SMART-FERT";
    }

    public DataAccess(string DBName, string ServerName, string UserId, string Pass)
    {
        StringBuilder sbConnectionString = new StringBuilder();
        sbConnectionString.Append("Persist Security Info=False;Initial Catalog=");
        sbConnectionString.Append(DBName.ToString());
        sbConnectionString.Append(";User Id=");
        sbConnectionString.Append(UserId.ToString());
        sbConnectionString.Append(";Password=");
        sbConnectionString.Append(Pass.ToString());
        sbConnectionString.Append(";Data Source=");
        sbConnectionString.Append(ServerName.ToString());
        m_sConnectionString = sbConnectionString.ToString();
    }

    #endregion

    public int InsertAdminEmail(string Address, string Password, string Host, int Port, int Timeout, bool EnableSsl)
    {
        int iID = -1;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminEmailInsert";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@Address", Address);
                    oCommand.Parameters.AddWithValue("@Password", Password);
                    oCommand.Parameters.AddWithValue("@Host", Host);
                    oCommand.Parameters.AddWithValue("@Port", Port);
                    oCommand.Parameters.AddWithValue("@Timeout", Timeout);
                    oCommand.Parameters.AddWithValue("@EnableSsl", EnableSsl);
                    SqlParameter nIdentity = new SqlParameter("@new_identity", SqlDbType.Int);
                    oCommand.Parameters.Add(nIdentity).Direction = ParameterDirection.Output;
                    iID = (int)oCommand.ExecuteScalar();
                }
            }
            catch (Exception Ex)
            {
                iID = -2;
            }
            finally
            {
                oConnection.Close();
            }
        return iID;
    }

    public bool UpdateAdminEmail(int EmailId, string Address, string Password, string Host, int Port, int Timeout, bool EnableSsl)
    {
        bool bResult = false;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminEmailUpdate";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@EmailId", EmailId);
                    oCommand.Parameters.AddWithValue("@Address", Address);
                    oCommand.Parameters.AddWithValue("@Password", Password);
                    oCommand.Parameters.AddWithValue("@Host", Host);
                    oCommand.Parameters.AddWithValue("@Port", Port);
                    oCommand.Parameters.AddWithValue("@Timeout", Timeout);
                    oCommand.Parameters.AddWithValue("@EnableSsl", EnableSsl);
                    int count = oCommand.ExecuteNonQuery();
                    if (count > 0)
                    {
                        bResult = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                bResult = false;
            }
            finally
            {
                oConnection.Close();
            }
        return bResult;
    }

    public bool DeleteAdminEmail(int EmailId)
    {
        bool bResult = false;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminEmailDelete";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@EmailId", EmailId);
                    int count = oCommand.ExecuteNonQuery();
                    if (count > 0)
                    {
                        bResult = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                bResult = false;
            }
            finally
            {
                oConnection.Close();
            }
        return bResult;
    }

    public DataTable SelectAllAdminEmail()
    {
        DataTable dT = new DataTable();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminEmailSelectAll";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    dataAdapt.SelectCommand = oCommand;
                    dataAdapt.Fill(dT);
                }
            }
            catch (Exception Ex)
            {
            }
            finally
            {
                oConnection.Close();
            }
        return dT;
    }

    public DataTable SelectAdminEmail(int EmailId)
    {
        DataTable dT = new DataTable();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminEmailSelect";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@EmailId", EmailId);
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    dataAdapt.SelectCommand = oCommand;
                    dataAdapt.Fill(dT);
                }
            }
            catch (Exception Ex)
            {
            }
            finally
            {
                oConnection.Close();
            }
        return dT;
    }

    public DataTable SelectAdminMessage(int MessageId)
    {
        DataTable dT = new DataTable();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminMessageSelect";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@MessageId", MessageId);
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    dataAdapt.SelectCommand = oCommand;
                    dataAdapt.Fill(dT);
                }
            }
            catch (Exception Ex)
            {
            }
            finally
            {
                oConnection.Close();
            }
        return dT;
    }

    public DataTable SelectAllAdminMessage()
    {
        DataTable dT = new DataTable();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminMessageSelectAll";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    dataAdapt.SelectCommand = oCommand;
                    dataAdapt.Fill(dT);
                }
            }
            catch (Exception Ex)
            {
            }
            finally
            {
                oConnection.Close();
            }
        return dT;
    }

    public int InsertAdminMessage(string subject, string body)
    {
        int iID = -1;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminMessageInsert";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@Subject", subject);
                    oCommand.Parameters.AddWithValue("@Body", body);
                    SqlParameter nIdentity = new SqlParameter("@new_identity", SqlDbType.Int);
                    oCommand.Parameters.Add(nIdentity).Direction = ParameterDirection.Output;
                    iID = (int)oCommand.ExecuteScalar();
                }
            }
            catch (Exception Ex)
            {
                iID = -2;
            }
            finally
            {
                oConnection.Close();
            }
        return iID;
    }

    public bool UpdateAdminMessage(int messageId, string subject, string body)
    {
        bool bResult = false;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminMessageUpdate";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@MessageId", messageId);
                    oCommand.Parameters.AddWithValue("@Subject", subject);
                    oCommand.Parameters.AddWithValue("@Body", body);
                    int count = oCommand.ExecuteNonQuery();
                    if (count > 0)
                    {
                        bResult = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                bResult = false;
            }
            finally
            {
                oConnection.Close();
            }
        return bResult;
    }

    public bool DeleteAdminMessage(int messageId)
    {
        bool bResult = false;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminMessageDelete";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@MessageId", messageId);
                    int count = oCommand.ExecuteNonQuery();
                    if (count > 0)
                    {
                        bResult = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                bResult = false;
            }
            finally
            {
                oConnection.Close();
            }
        return bResult;
    }


    public DataTable SelectCropData()
    {
        DataTable dT = new DataTable();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            //try
            //{
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "SELECT cast(B.EntityData as xml) .value('(/Translation/@Name)[1]', 'nvarchar(max)' ) as Esp ,* FROM [dbo].[Crop] A inner join [dbo].[SystemEntityTranslation] B on A.[CropId] = B.Id where A.[IsSystemCrop] = 1 and B.[EnityType] = 2";
                    oCommand.CommandType = CommandType.Text;
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    dataAdapt.SelectCommand = oCommand;
                    dataAdapt.Fill(dT);
                }
            //}
            //catch (Exception Ex)
            //{
            //}
            //finally
            //{
               // oConnection.Close();
            //}
        return dT;
    }

    public DataTable SelectCropData2()
    {
        DataTable dT = new DataTable();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
        //try
        //{
        using (SqlCommand oCommand = oConnection.CreateCommand())
        {
            oConnection.Open();
            oCommand.CommandText = " SELECT cast(B.EntityData as xml).value('(/Crop/@Name)[1]', 'nvarchar(max)' ) as Rus ,*  FROM  [dbo].[SystemEntityTranslation2] B ";
            oCommand.CommandType = CommandType.Text;
            SqlDataAdapter dataAdapt = new SqlDataAdapter();
            dataAdapt.SelectCommand = oCommand;
            dataAdapt.Fill(dT);
        }
        //}
        //catch (Exception Ex)
        //{
        //}
        //finally
        //{
        // oConnection.Close();
        //}
        return dT;
    }

    public bool UpdateCropRus(int CropId, string RusCropName)
    {
        bool bResult = false;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminTestCropUpdate";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@CropId", CropId);
                    oCommand.Parameters.AddWithValue("@RusCropName", RusCropName);
                    int count = oCommand.ExecuteNonQuery();
                    if (count > 0)
                    {
                        bResult = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                bResult = false;
            }
            finally
            {
                oConnection.Close();
            }
        return bResult;
    }


    public bool UpdateCropEsp(int CropId, string EspCropName)
    {
        bool bResult = false;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminTestCropUpdate";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@CropId", CropId);
                    oCommand.Parameters.AddWithValue("@EspCropName", EspCropName);
                    int count = oCommand.ExecuteNonQuery();
                    if (count > 0)
                    {
                        bResult = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                bResult = false;
            }
            finally
            {
                oConnection.Close();
            }
        return bResult;
    }

    public int InsertEntityTranslation(int EntityId, int EnityType, string EntityData, int LanguageId)
    {
        int iID = -1;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stpAdminInsertEntityTranslation";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@EntityId", EntityId);
                    oCommand.Parameters.AddWithValue("@EnityType", EnityType);
                    oCommand.Parameters.AddWithValue("@EntityData", EntityData);
                    oCommand.Parameters.AddWithValue("@LanguageId", LanguageId);
                    //SqlParameter nIdentity = new SqlParameter("@new_identity", SqlDbType.Int);
                    //oCommand.Parameters.Add(nIdentity).Direction = ParameterDirection.Output;
                    oCommand.ExecuteScalar();
                }
            }
            catch (Exception Ex)
            {
                iID = -2;
            }
            finally
            {
                oConnection.Close();
            }
        return iID;
    }


    public DataTable SelectAllTransalation()
    {
        DataTable dT = new DataTable();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "SELECT * FROM [dbo].[_SystemTransalationApplictionLabels]";
                   // oCommand.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    dataAdapt.SelectCommand = oCommand;
                    dataAdapt.Fill(dT);
                }
            }
            catch (Exception Ex)
            {
            }
            finally
            {
                oConnection.Close();
            }
        return dT;
    }


    public DataTable SearchTransalation(string searchparam)
    {
        DataTable dT = new DataTable();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "SELECT * FROM [dbo].[_SystemTransalationApplictionLabels] WHERE [LabelName] like '%" + searchparam + "%' or [EngLabelName] like '%" + searchparam + "%' or [EspLabelName] like '%" + searchparam + "%' or [HebLabelName] like N'%" + searchparam + "%' or [ScreenName] like '%" + searchparam + "%' or [PosistionOnScreen] like '%" + searchparam + "%'";
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    dataAdapt.SelectCommand = oCommand;
                    dataAdapt.Fill(dT);
                }
            }
            catch (Exception Ex)
            {
            }
            finally
            {
                oConnection.Close();
            }
        return dT;
    }


    public bool UpdateTranslationLabels(int SystemTransiD 
	  ,int ModuleId
      ,string ScreenName
      ,string PosistionOnScreen
      ,string EngLabelName
      ,bool IsEngLabelActive
      ,string EspLabelName
      ,bool IsEspLabelActive 
      //,string FraLabelName 
      //,bool IsFraLabelActive 
      ,string HebLabelName 
      ,bool IsHebLabelActive 
      //,string GrcLabelName 
      //,bool IsGrcLabelActive 
      //,string ItaLabelName
      //,bool IsItaLabelActive 
      //,string PorLabelName 
      //,bool IsPorLabelActive 
      //,string PolLabelName 
      //,bool IsPolLabelActive
      //,string RusLabelName 
      //,bool IsRusLabelActive
        )
    {
        bool bResult = false;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "sp_TransApplictionLabelsUpdate";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@SystemTransiD", SystemTransiD);
                    oCommand.Parameters.AddWithValue("@ModuleId", ModuleId);
                    oCommand.Parameters.AddWithValue("@ScreenName", ScreenName);
                    oCommand.Parameters.AddWithValue("@PosistionOnScreen", PosistionOnScreen);
                    oCommand.Parameters.AddWithValue("@EngLabelName", EngLabelName);
                    oCommand.Parameters.AddWithValue("@IsEngLabelActive", IsEngLabelActive);
                    oCommand.Parameters.AddWithValue("@EspLabelName", EspLabelName);
                    oCommand.Parameters.AddWithValue("@IsEspLabelActive", IsEspLabelActive);
                    //oCommand.Parameters.AddWithValue("@FraLabelName", FraLabelName);
                    //oCommand.Parameters.AddWithValue("@IsFraLabelActive", IsFraLabelActive);
                    oCommand.Parameters.AddWithValue("@HebLabelName", HebLabelName);
                    oCommand.Parameters.AddWithValue("@IsHebLabelActive", IsHebLabelActive);
                    //oCommand.Parameters.AddWithValue("@GrcLabelName", GrcLabelName);
                    //oCommand.Parameters.AddWithValue("@IsGrcLabelActive", IsGrcLabelActive);
                    //oCommand.Parameters.AddWithValue("@ItaLabelName", ItaLabelName);
                    //oCommand.Parameters.AddWithValue("@IsItaLabelActive", IsItaLabelActive);
                    //oCommand.Parameters.AddWithValue("@PorLabelName", PorLabelName);
                    //oCommand.Parameters.AddWithValue("@IsPorLabelActive", IsPorLabelActive);
                    //oCommand.Parameters.AddWithValue("@PolLabelName", PolLabelName);
                    //oCommand.Parameters.AddWithValue("@IsPolLabelActive", IsPolLabelActive);
                    //oCommand.Parameters.AddWithValue("@RusLabelName", RusLabelName);
                    //oCommand.Parameters.AddWithValue("@IsRusLabelActive", IsRusLabelActive);

                    int count = oCommand.ExecuteNonQuery();
                    if (count > 0)
                    {
                        bResult = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                bResult = false;
            }
            finally
            {
                oConnection.Close();
            }
        return bResult;
    }


    public int InsertTranslationLabel(int ModuleId
      ,string ScreenName
      ,string PosistionOnScreen
      ,string LabelName
      ,string EngLabelName
      ,bool IsEngLabelActive
      ,string EspLabelName
      ,bool IsEspLabelActive
      , string HebLabelName
      , bool IsHebLabelActive)
    {
        int iID = -1;
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "sp_TransApplictionLabelsInsert";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@ModuleId", ModuleId);
                    oCommand.Parameters.AddWithValue("@ScreenName", (object)ScreenName ?? DBNull.Value);
                    oCommand.Parameters.AddWithValue("@PosistionOnScreen", (object)PosistionOnScreen ?? DBNull.Value);
                    oCommand.Parameters.AddWithValue("@LabelName", LabelName);
                    oCommand.Parameters.AddWithValue("@EngLabelName", EngLabelName);
                    oCommand.Parameters.AddWithValue("@IsEngLabelActive", IsEngLabelActive);
                    oCommand.Parameters.AddWithValue("@EspLabelName", (object)EspLabelName ?? DBNull.Value);
                    oCommand.Parameters.AddWithValue("@IsEspLabelActive", IsEspLabelActive);
                    oCommand.Parameters.AddWithValue("@HebLabelName", (object)HebLabelName ?? DBNull.Value);
                    oCommand.Parameters.AddWithValue("@IsHebLabelActive", IsHebLabelActive);

                    SqlParameter nIdentity = new SqlParameter("@new_identity", SqlDbType.Int);
                    oCommand.Parameters.Add(nIdentity).Direction = ParameterDirection.Output;
                    iID = (int)oCommand.ExecuteScalar();
                }
            }
            catch (Exception Ex)
            {
                iID = -2;
            }
            finally
            {
                oConnection.Close();
            }
        return iID;
    }




    public DataTable SelectICLFert()
    {
        DataTable dT = new DataTable();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionString))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "SELECT * FROM [dbo].[FertICL]";
                    // oCommand.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    dataAdapt.SelectCommand = oCommand;
                    dataAdapt.Fill(dT);
                }
            }
            catch (Exception Ex)
            {
            }
            finally
            {
                oConnection.Close();
            }
        return dT;
    }


    public DataTable SelectOldImageFiles()
    {
        DataTable dT = new DataTable();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionStringProd))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stp_FileUpload_Test";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    dataAdapt.SelectCommand = oCommand;
                    dataAdapt.Fill(dT);
                }
            }
            catch (Exception Ex)
            {
            }
            finally
            {
                oConnection.Close();
            }
        return dT;
    }


    public Guid InsertFileUpload(
                int UserId,
                string FileUploadName,
                string FileUploadDescription,
                int EntityTypeId,
                int EntityId,
                int ParentEntityId,
                int FileExtensionId
        )
    {
        Guid UniqueId = new Guid();
        using (SqlConnection oConnection = new SqlConnection(m_sConnectionStringProd))
            try
            {
                using (SqlCommand oCommand = oConnection.CreateCommand())
                {
                    oConnection.Open();
                    oCommand.CommandText = "_stp_FileUpload_Insert";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@UserId", UserId);
                    oCommand.Parameters.AddWithValue("@FileUploadName", FileUploadName);
                    oCommand.Parameters.AddWithValue("@FileUploadDescription", FileUploadDescription);
                    oCommand.Parameters.AddWithValue("@EntityTypeId", EntityTypeId);
                    oCommand.Parameters.AddWithValue("@EntityId", EntityId);
                    oCommand.Parameters.AddWithValue("@ParentEntityId", ParentEntityId);
                    oCommand.Parameters.AddWithValue("@FileExtensionId", FileExtensionId);

                    SqlParameter nIdentity = new SqlParameter("@new_identity", SqlDbType.Int);
                    oCommand.Parameters.Add(nIdentity).Direction = ParameterDirection.Output;
                    SqlParameter nUniqueId = new SqlParameter("@UniqueId", SqlDbType.UniqueIdentifier);
                    oCommand.Parameters.Add(nUniqueId).Direction = ParameterDirection.Output;
                   oCommand.ExecuteScalar();
                   UniqueId = (Guid)nUniqueId.Value;
                }
            }
            catch (Exception Ex)
            {
                UniqueId = Guid.Empty;
            }
            finally
            {
                oConnection.Close();
            }
        return UniqueId;
    }


    public DataTable GetDBList()
    {
        DataTable dtDatabases;
        using (var connection = new System.Data.SqlClient.SqlConnection(m_sConnectionString))
        {
            connection.Open();
            var command = new System.Data.SqlClient.SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.Text;
            command.CommandText = "SELECT * FROM master.sys.databases";

            var adapter = new System.Data.SqlClient.SqlDataAdapter(command);
            var dataset = new DataSet();
            adapter.Fill(dataset);
            dtDatabases = dataset.Tables[0];
        }
        return dtDatabases;
    }


}