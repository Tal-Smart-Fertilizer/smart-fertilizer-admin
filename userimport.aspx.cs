﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExcelWebForms;
using OfficeOpenXml;
using System.Data;
using Facade;
using System.Text;
using Smart.Entities;
using Smart.DataManager;
using System.Web.Security;
using System.Web.Script.Serialization;

public partial class userimport : System.Web.UI.Page
{
    public string[] columnNames;
    //public DataTable UploadedTable;
    //DataTable dtColumnOrder = new DataTable();
    DataTable dtColumnOrder = new DataTable();
    DataTable dtUploadedExel = null;

    #region Init Countries
    Dictionary<string, string> arrCountry = new Dictionary<string, string>()
        {
             {"Andorra","AD"},
            {"United Arab Emirates","AE"},
            {"Afghanistan","AF"},
            {"Antigua and Barbuda","AG"},
            {"Anguilla","AI"},
            {"Albania","AL"},
            {"Armenia","AM"},
            {"Angola","AO"},
            {"Antarctica","AQ"},
            {"Argentina","AR"},
            {"Austria","AT"},
            {"Australia","AU"},
            {"Aruba","AW"},
            {"Aland Islands","AX"},
            {"Azerbaijan","AZ"},
            {"Bosnia and Herzegovina","BA"},
            {"Barbados","BB"},
            {"Bangladesh","BD"},
            {"Belgium","BE"},
            {"Burkina Faso","BF"},
            {"Bulgaria","BG"},
            {"Bahrain","BH"},
            {"Burundi","BI"},
            {"Benin","BJ"},
            {"Saint Barthélemy","BL"},
            {"Bermuda","BM"},
            {"Brunei Darussalam","BN"},
            {"Bolivia, Plurinational State of","BO"},
            {"Bonaire, Sint Eustatius and Saba","BQ"},
            {"Brazil","BR"},
            {"Bahamas","BS"},
            {"Bhutan","BT"},
            {"Bouvet Island","BV"},
            {"Botswana","BW"},
            {"Belarus","BY"},
            {"Belize","BZ"},
            {"Canada","CA"},
            {"Cocos (Keeling) Islands","CC"},
            {"Congo, the Democratic Republic of the","CD"},
            {"Central African Republic","CF"},
            {"Congo","CG"},
            {"Switzerland","CH"},
            {"Cote d'Ivoire","CI"},
            {"Cook Islands","CK"},
            {"Chile","CL"},
            {"Cameroon","CM"},
            {"China","CN"},
            {"Colombia","CO"},
            {"Costa Rica","CR"},
            {"Cuba","CU"},
            {"Cape Verde","CV"},
            {"Curaçao"," CW"},
            {"Christmas Island","CX"},
            {"Cyprus","CY"},
            {"Czech Republic","CZ"},
            {"Germany","DE"},
            {"Djibouti","DJ"},
            {"Denmark","DK"},
            {"Dominica","DM"},
            {"Dominican Republic","DO"},
            {"Algeria","DZ"},
            {"Ecuador","EC"},
            {"Estonia","EE"},
            {"Egypt","EG"},
            {"Western Sahara","EH"},
            {"Eritrea","ER"},
            {"Spain","ES"},
            {"Ethiopia","ET"},
            {"Finland","FI"},
            {"Fiji","FJ"},
            {"Falkland Islands (Malvinas)","FK"},
            {"Faroe Islands","FO"},
            {"France","FR"},
            {"Gabon","GA"},
            {"United Kingdom","GB"},
            {"Grenada","GD"},
            {"Georgia","GE"},
            {"French Guiana","GF"},
            {"Guernsey","GG"},
            {"Ghana","GH"},
            {"Gibraltar","GI"},
            {"Greenland","GL"},
            {"Gambia","GM"},
            {"Guinea","GN"},
            {"Guadeloupe","GP"},
            {"Equatorial Guinea","GQ"},
            {"Greece","GR"},
            {"South Georgia and the South Sandwich Islands","GS"},
            {"Guatemala","GT"},
            {"Guinea-Bissau","GW"},
            {"Guyana","GY"},
            {"Heard Island and McDonald Islands","HM"},
            {"Honduras","HN"},
            {"Croatia","HR"},
            {"Haiti","HT"},
            {"Hungary","HU"},
            {"Indonesia","ID"},
            {"Ireland","IE"},
            {"Israel","IL"},
            {"Isle of Man","IM"},
            {"India","IN"},
            {"British Indian Ocean Territory","IO"},
            {"Iraq","IQ"},
            {"Iran","IR"},
            {"Iceland","IS"},
            {"Italy","IT"},
            {"Jersey","JE"},
            {"Jamaica","JM"},
            {"Jordan","JO"},
            {"Japan","JP"},
            {"Kenya","KE"},
            {"Kyrgyzstan","KG"},
            {"Cambodia","KH"},
            {"Kiribati","KI"},
            {"Comoros","KM"},
            {"Saint Kitts and Nevis","KN"},
            {"Korea, Democratic People's Republic of","KP"},
            {"Korea, Republic of","KR"},
            {"Kuwait","KW"},
            {"Cayman Islands","KY"},
            {"Kazakhstan","KZ"},
            {"Lao People's Democratic Republic","LA"},
            {"Lebanon","LB"},
            {"Saint Lucia","LC"},
            {"Liechtenstein","LI"},
            {"Sri Lanka","LK"},
            {"Liberia","LR"},
            {"Lesotho","LS"},
            {"Lithuania","LT"},
            {"Luxembourg","LU"},
            {"Latvia","LV"},
            {"Libyan Arab Jamahiriya","LY"},
            {"Morocco","MA"},
            {"Monaco","MC"},
            {"Moldova, Republic of","MD"},
            {"Montenegro","ME"},
            {"Saint Martin (French part)","MF"},
            {"Madagascar","MG"},
            {"Macedonia, the former Yugoslav Republic of","MK"},
            {"Mali","ML"},
            {"Myanmar","MM"},
            {"Mongolia","MN"},
            {"Macao","MO"},
            {"Martinique","MQ"},
            {"Mauritania","MR"},
            {"Montserrat","MS"},
            {"Malta","MT"},
            {"Mauritius","MU"},
            {"Maldives","MV"},
            {"Malawi","MW"},
            {"Mexico","MX"},
            {"Malaysia","MY"},
            {"Mozambique","MZ"},
            {"Namibia","NA"},
            {"New Caledonia","NC"},
            {"Niger","NE"},
            {"Norfolk Island","NF"},
            {"Nigeria","NG"},
            {"Nicaragua","NI"},
            {"Netherlands","NL"},
            {"Norway","NO"},
            {"Nepal","NP"},
            {"Nauru","NR"},
            {"Niue","NU"},
            {"New Zealand","NZ"},
            {"Oman","OM"},
            {"Panama","PA"},
            {"Peru","PE"},
            {"French Polynesia","PF"},
            {"Papua New Guinea","PG"},
            {"Philippines","PH"},
            {"Pakistan","PK"},
            {"Poland","PL"},
            {"Saint Pierre and Miquelon","PM"},
            {"Pitcairn","PN"},
            {"Palestinian Territory, Occupied","PS"},
            {"Portugal","PT"},
            {"Paraguay","PY"},
            {"Qatar","QA"},
            {"Reunion","RE"},
            {"Romania","RO"},
            {"Serbia","RS"},
            {"Russian Federation","RU"},
            {"Rwanda","RW"},
            {"Saudi Arabia","SA"},
            {"Solomon Islands","SB"},
            {"Seychelles","SC"},
            {"Sudan","SD"},
            {"Sweden","SE"},
            {"Singapore","SG"},
            {"Saint Helena, Ascension and Tristan da Cunha","SH"},
            {"Slovenia","SI"},
            {"Svalbard and Jan Mayen","SJ"},
            {"Slovakia","SK"},
            {"Sierra Leone","SL"},
            {"San Marino","SM"},
            {"Senegal","SN"},
            {"Somalia","SO"},
            {"Suriname","SR"},
            {"South Sudan","SS"},
            {"Sao Tome and Principe","ST"},
            {"El Salvador","SV"},
            {"Sint Maarten (Dutch part)","SX"},
            {"Syrian Arab Republic","SY"},
            {"Swaziland","SZ"},
            {"Turks and Caicos Islands","TC"},
            {"Chad","TD"},
            {"French Southern Territories","TF"},
            {"Togo","TG"},
            {"Thailand","TH"},
            {"Tajikistan","TJ"},
            {"Tokelau","TK"},
            {"Timor-Leste","TL"},
            {"Turkmenistan","TM"},
            {"Tunisia","TN"},
            {"Tonga","TO"},
            {"Turkey","TR"},
            {"Trinidad and Tobago","TT"},
            {"Tuvalu","TV"},
            {"Chinese Taipei","TW"},
            {"Tanzania, United Republic of","TZ"},
            {"Ukraine","UA"},
            {"Uganda","UG"},
            {"United States","US"},
            {"Uruguay","UY"},
            {"Uzbekistan","UZ"},
            {"Holy See (Vatican City State)","VA"},
            {"Saint Vincent and the Grenadines","VC"},
            {"Venezuela, Bolivarian Republic of","VE"},
            {"Virgin Islands, British","VG"},
            {"Viet Nam","VN"},
            {"Vanuatu","VU"},
            {"Wallis and Futuna","WF"},
            {"Samoa","WS"},
            {"Yemen","YE"},
            {"Mayotte","YT"},
            {"South Africa","ZA"},
            {"Zambia","ZM"},
            {"Zimbabwe","ZW"}
            ,{"Venezuela","VE"}
            ,{"Trinidad y Tobago","TT"}
            ,{"Sao Tome","ST"}
            ,{"República Dominicana","DO"}
            ,{"Puerto Rico","PR"}
            ,{"Estaos Unidos","US"}
            ,{"España","ES"}
            

        };
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dresult.Visible = false;
        }
    }


    private void loadDynamicGrid(DataTable dt)
    {
        //Create DataTable with upload grid columns

        dtColumnOrder.Clear();
        dtColumnOrder.Columns.Add("SortOrder");
        dtColumnOrder.Columns.Add("ColumnName");
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            DataRow dr = dtColumnOrder.NewRow();
            dr["SortOrder"] = i;
            dr["ColumnName"] = dt.Columns[i].ColumnName.ToString();
            dtColumnOrder.Rows.Add(dr);
        }
        //
        DataTable dtCol = CreateUserTable();


        for (int nIndex = 0; nIndex < 10; nIndex++)
        {
            //Create a new row
            DataRow drow = dtCol.NewRow();

            //Initialize the row data.
            drow[3] = "Row-" + Convert.ToString((nIndex + 1));

            //Add the row to the datatable.
            dtCol.Rows.Add(drow);
        }

        //foreach (DataColumn col in dtCol.Columns)
        //{
        //    //Declare the bound field and allocate memory for the bound field.
        //    BoundField bfield = new BoundField();

        //    //Initalize the DataField value.
        //    bfield.DataField = col.ColumnName;

        //    //Initialize the HeaderText field value.
        //    bfield.HeaderText = col.ColumnName;

        //    //Add the newly created bound field to the GridView.
        //    gvMapping.Columns.Add(bfield);
        //}



        // Bind data to grid
        gvMapping.DataSource = dtCol;
        gvMapping.DataBind();
    }

    private DataTable CreateUserTable()
    {
        DataTable dtUserTable = new DataTable();
        DataColumn dcol = new DataColumn("UserId", typeof(System.Int32));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("Language", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("UIDirection", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("UITheme", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("OrganizationId", typeof(System.Int32));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("DateFormat", typeof(System.Int32));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("DateSeparator", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("UserCaption", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("UIZoom", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("FarmPreferredUnits", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("UserName", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("Password", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("IsActive", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("PasswordExpirationDate", typeof(System.DateTime));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("ConfirmRegistration", typeof(System.Int32));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("UpdatedByUserId", typeof(System.Int32));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("UpdatedDate", typeof(System.DateTime));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("CreatedByUserId", typeof(System.Int32));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("CreatedDate", typeof(System.DateTime));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("UserType", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("FirstName", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("LastName", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("IsLocked", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("ValidUntil", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("FarmId", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("LoggedIn", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("Country", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("AccountType", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("LastLoginDate", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("LastLoginAttemptDate", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("LastLoginAttemptDat", typeof(System.DateTime));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("LastLoginUserIp", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("Email", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("LanguageId", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("UIDateFormat", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("Status", typeof(System.Int32));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("FarmName", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("CreateFarm", typeof(System.Int32));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("State", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("City", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("Zip", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("Address", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("Phone", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("Cellular", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        dcol = new DataColumn("Currency", typeof(System.String));
        dtUserTable.Columns.Add(dcol);
        return dtUserTable;
    }


    protected void Upload_Click(object sender, EventArgs e)
    {
        if (this.FileUpload1.HasFile)
        {
            if (Path.GetExtension(FileUpload1.FileName) == ".xlsx")
            {
                dtUploadedExel = new DataTable();
                ExcelPackage package = new ExcelPackage(FileUpload1.FileContent);
                dtUploadedExel = package.ToDataTable();

                //loadDynamicGrid(dtUploadedExel);
                Session.Remove("gvImport");
                Session.Add("gvImport", dtUploadedExel);

                gvImport.DataSource = dtUploadedExel;
                gvImport.DataBind();

                btnBulkInsert.Visible = true;
            }
        }
    }

    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {
        switch (e.Item.Value)
        {
            case "T1":
                MultiView1.ActiveViewIndex = 0;
                break;
            case "T2":
                MultiView1.ActiveViewIndex = 1;
                break;
            case "T3":
                MultiView1.ActiveViewIndex = 2;
                break;
        }
    }


    protected void odsEmailsDetails_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        gvEmails.DataBind();
    }
    protected void odsEmailsDetails_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        gvEmails.DataBind();
    }
    protected void odsEmailsDetails_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        gvEmails.DataBind();
        dvEmails.DataBind();
    }
    protected void odsMessagesList_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        gvMessages.DataBind();
        dvMessageItem.DataBind();
    }
    protected void odsMessagesItem_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        gvMessages.DataBind();
    }
    protected void odsMessagesItem_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        gvMessages.DataBind();
    }

    protected void gvMapping_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Finding the Dropdown control.
            Control ctrl = e.Row.FindControl("ddlUploadedFields");
            if (ctrl != null)
            {
                DropDownList dd = ctrl as DropDownList;

                dd.DataTextField = "ColumnName";
                dd.DataValueField = "SortOrder";
                dd.DataSource = dtColumnOrder;
                dd.DataBind();
            }
        }
    }
    protected void gvMessages_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var ddl = e.Row.FindControl("ddlAvailableEmails") as DropDownList;
            if (ddl != null)
            {

                DataTable dtAvailableMail = new DataTable();
                BusinessLogic oLogic = new BusinessLogic();
                dtAvailableMail = oLogic.SelectAllAdminEmail();

                ddl.DataSource = dtAvailableMail; //odsEmailsList.Select(); //new List<string>() { "0", "1", "2", "3", "4" };
                ddl.DataValueField = "EmailId";
                ddl.DataTextField = "Address";

                ddl.DataBind();
            }
        }
    }
    protected void gvMessages_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "sendtest")
        {
            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            StringBuilder sbBody = new StringBuilder();

            string sSubject = gvMessages.Rows[rowIndex].Cells[2].Text;
            sbBody.Append(HttpUtility.HtmlDecode(gvMessages.Rows[rowIndex].Cells[3].Text));
            //
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            DropDownList lblProdId = (DropDownList)row.FindControl("ddlAvailableEmails");
            int iEmailId = Int32.Parse(lblProdId.SelectedItem.Value);

            TextBox tbRecipient = (TextBox)row.FindControl("tbRecipientMail");
            string sRecipient = tbRecipient.Text.ToString();
            //
            BusinessLogic oLogic = new BusinessLogic();
            DataTable dtEmailData = oLogic.SelectAdminEmail(iEmailId);
            //
            EmailManagement oEmail = new EmailManagement();
            oEmail.Username = dtEmailData.Rows[0]["Address"].ToString();
            oEmail.Password = dtEmailData.Rows[0]["Password"].ToString();
            oEmail.SmtpServer = dtEmailData.Rows[0]["Host"].ToString();
            oEmail.From = dtEmailData.Rows[0]["Address"].ToString();
            oEmail.EnableSsl = Convert.ToBoolean(dtEmailData.Rows[0]["EnableSsl"].ToString());
            oEmail.Port = Int32.Parse(dtEmailData.Rows[0]["Port"].ToString());
            oEmail.Timeout = Int32.Parse(dtEmailData.Rows[0]["Timeout"].ToString());

            oEmail.SendMail(sRecipient, sSubject, sbBody.ToString(), null);

        }
    }


    protected void btnBulkInsert_Click(object sender, EventArgs e)
    {
        string sDefaultLanguage = ddlDefaultLanguage.SelectedValue;
        DataTable dt = (DataTable)Session["gvImport"];//GetDataTable(gvImport);
        string sFirstName = string.Empty;
        string sLastName = string.Empty;
        string sPhone = string.Empty;
        string sPass = string.Empty;
        int newUserId = -1; // change to -1
        User oUser = null;

        DataTable dtFalseResult = dt.Clone();
        DataTable dtTrueResult = dt.Clone();

        dtTrueResult.Columns.Add("Password");
        dtTrueResult.Columns.Add("Id");
        //
        dtFalseResult.Columns.Add("Exception");


        foreach (DataRow dataRow in dt.Rows)
        {
            try
            {
                //validation
                if (string.IsNullOrEmpty(dataRow["FirstName"].ToString()) || 
                    string.IsNullOrEmpty(dataRow.IsNull("LastName").ToString()) || 
                    string.IsNullOrEmpty(dataRow.IsNull("Email").ToString()) || 
                    string.IsNullOrEmpty(dataRow.IsNull("Phone").ToString())) break;

                oUser = new User();
                oUser.OrganizationId = 0;
                //
                oUser.UserName = dataRow["Email"].ToString(); // required / calc
  
                if (dataRow["FirstName"].ToString().Any(Char.IsWhiteSpace))
                {
                    int index = dataRow["FirstName"].ToString().IndexOf(' ');
                    index = dataRow["FirstName"].ToString().IndexOf(' ', index);
                    sFirstName = dataRow["FirstName"].ToString().Substring(0, index).ToLower();
                }
                else
                {
                    sFirstName = dataRow["FirstName"].ToString().ToLower();
                }

                if (dataRow["LastName"].ToString().Any(Char.IsWhiteSpace))
                {
                    int index = dataRow["LastName"].ToString().IndexOf(' ');
                    index = dataRow["LastName"].ToString().IndexOf(' ', index);
                    sLastName = dataRow["LastName"].ToString().Substring(0, index).ToLower();
                }
                else 
                {
                    sLastName = dataRow["LastName"].ToString().ToLower();
                }

                sPhone =  dataRow["Phone"].ToString().Substring(dataRow["Phone"].ToString().Length - Math.Min(4, dataRow["Phone"].ToString().Length)).Trim().ToLower(); // required  / calc

                //oUser.Password = Smart.Common.Md5Hash.CalculateMd5Hash((dataRow["FirstName"].ToString().Substring(0, 1) + 
                //    dataRow["LastName"].ToString() + 
                //    dataRow["Phone"].ToString().Substring(dataRow["Phone"].ToString().Length - 
                //    Math.Min(4, dataRow["Phone"].ToString().Length))).Trim().ToLower()); // required  / calc

                sPass = sFirstName[0].ToString() + sLastName + sPhone;
                oUser.Password = Smart.Common.Md5Hash.CalculateMd5Hash(sPass);

                oUser.FirstName = Helper.CapitalizeWords(dataRow["FirstName"].ToString()); // required
                oUser.LastName = Helper.CapitalizeWords(dataRow["LastName"].ToString()); // required
                oUser.Email = dataRow["Email"].ToString();  // required
                oUser.Phone = dataRow["Phone"].ToString(); // required

                try
                {
                    if (dt.Columns.Contains("Country"))
                    {
                        oUser.Country = (arrCountry.FirstOrDefault(x => x.Key == dataRow["Country"].ToString()).Value).ToString().Trim();
                    }
                }
                catch
                {

                }
                //
                if (dt.Columns.Contains("IsActive"))
                {
                    oUser.IsActive = dataRow["IsActive"].ToString();
                }
                else
                {
                    oUser.IsActive = "1";
                }
                if (dt.Columns.Contains("IsLocked"))
                {
                    oUser.IsLocked = dataRow["IsLocked"].ToString();
                }
                else
                {
                    oUser.IsLocked = "0";
                }

                oUser.LanguageId = sDefaultLanguage;

                if (dt.Columns.Contains("UIDateFormat"))
                {
                    oUser.UIDateFormat = dataRow["UIDateFormat"].ToString();
                }
                else
                {
                    oUser.UIDateFormat = "2";
                }

                oUser.FarmId = "0";
                oUser.Id = 0;

                if (dt.Columns.Contains("DateFormat"))
                {
                    oUser.DateFormat = Int32.Parse(dataRow["DateFormat"].ToString());
                }
                else
                {
                    oUser.DateFormat = 0;
                }

                //oUser.Language 
                //oUser.LanguageId
                try
                {
                    if (oUser.Country == "US" || oUser.Country == "CA")
                    {
                        oUser.FarmPreferredUnits = "US";
                    }
                    else
                    {
                        oUser.FarmPreferredUnits = "ME";
                    }
                    //if (dt.Columns.Contains("FarmPreferredUnits"))
                    //{
                    //    oUser.FarmPreferredUnits = dataRow["FarmPreferredUnits"].ToString();
                    //}
                    //else
                    //{
                    //    oUser.FarmPreferredUnits = "ME";
                    //}
                }
                catch { 
                
                }

                DateTime time = DateTime.Now.AddDays(7);    // Use current time
                string format = "yyyy/M/d HH:mm:ss.mmm";    // Use this format

                if (dt.Columns.Contains("ValidUntil"))
                {
                    time = DateTime.Parse(dataRow["ValidUntil"].ToString());
                    oUser.ValidUntil = time.ToString(format);
                }
                else
                {
                    oUser.ValidUntil = time.ToString(format);
                }
                //
                if (dt.Columns.Contains("State"))
                {
                    oUser.State = dataRow["State"].ToString();
                }
                if (dt.Columns.Contains("City"))
                {
                    oUser.City = dataRow["City"].ToString();
                }
                if (dt.Columns.Contains("Zip"))
                {
                    oUser.Zip = dataRow["Zip"].ToString();
                }
                if (dt.Columns.Contains("Address"))
                {
                    oUser.Address = dataRow["Address"].ToString();
                }
                if (dt.Columns.Contains("Cellular"))
                {
                    oUser.Cellular = dataRow["Cellular"].ToString();
                }
                if (dt.Columns.Contains("Currency"))
                {
                    oUser.Currency = dataRow["Currency"].ToString();
                }
                //

                oUser.CreateFarm = 1;
                oUser.FarmName = "Default";
                //
                int iUserId = 18; //change user
                string sToken = string.Empty;

                if (Session["userid"] != null)
                {
                    iUserId = Int32.Parse(Session["userid"].ToString());
                }

                if (Session["token"] != null)
                {
                    sToken = Session["token"].ToString();
                }

                newUserId = Smart.DataManager.ManageUser.addUser(iUserId, oUser);
                Smart.DataManager.ManageUser.UpdateFarmUserId(newUserId, sToken);
                ManageRestriction.InsertUserRestriction(new Restriction() { Crop = 2, Fertilizer = 2, Program = 2, SoilTest = 2, TissueTest = 2, TotalPlotArea = 100, UserId = newUserId, WaterTest = 2 });

                dtTrueResult.ImportRow(dataRow);
                //int rIndex = dtTrueResult.Rows.IndexOf(dataRow);
                int rIndex = dtTrueResult.Rows.Count - 1;

                dtTrueResult.Rows[rIndex]["Id"] = newUserId.ToString();
                dtTrueResult.Rows[rIndex]["Password"] = sPass;


                //System.Data.DataTable test;
                //test.importRows(datarow);
                //test.Rows.IndexOf(datarow);

            }
            catch (Exception ex)
            {
                dtFalseResult.ImportRow(dataRow);
                //int rIndex = dtFalseResult.Rows.IndexOf(dataRow);
                int rIndex = dtFalseResult.Rows.Count - 1;

                dtFalseResult.Rows[rIndex]["Exception"] = ex.ToString();
            }
        }


        dresult.Visible = true;

        gvRightResult.DataSource = dtTrueResult;
        gvRightResult.DataBind();

        gvFalseResult.DataSource = dtFalseResult;
        gvFalseResult.DataBind();

    }

    DataTable GetDataTable(GridView dtg)
    {
        DataTable dt = new DataTable();
        // add the columns to the datatable            
        if (dtg.HeaderRow != null)
        {

            for (int i = 0; i < dtg.HeaderRow.Cells.Count; i++)
            {
                dt.Columns.Add(dtg.HeaderRow.Cells[i].Text);
            }
        }
        //  add each of the data rows to the table
        foreach (GridViewRow row in dtg.Rows)
        {
            DataRow dr;
            dr = dt.NewRow();

            for (int i = 0; i < row.Cells.Count; i++)
            {
                dr[i] = row.Cells[i].Text.Replace(" ", "");
            }
            dt.Rows.Add(dr);
        }
        return dt;
    }

    protected void btnSignOut_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
        {
            HttpCookie myCookie = new HttpCookie(FormsAuthentication.FormsCookieName);
            //myCookie.Expires = DateTime.Now.AddDays(-1d);
            myCookie.Expires = DateTime.Now.AddMinutes(50);
            Response.Cookies.Add(myCookie);
            FormsAuthentication.SignOut();
            //
            Session.Remove("userid");
            Session.Remove("token");
        }
        Response.Redirect("Login.aspx");
    }

    protected void btnTranslation_Click(object sender, EventArgs e)
    {
        Response.Redirect("Translation.aspx");
    }
}