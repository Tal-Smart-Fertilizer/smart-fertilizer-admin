﻿using Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Smart.DataManager;
using Smart.Entities;
using System.Data;
using System.Web.Security;

public partial class Translation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
    }
    protected void bnCleart_Click(object sender, EventArgs e)
    {
        tbSearch.Text = string.Empty;
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        if (dvTransalation.Visible == false)
        {
            dvTransalation.Visible = true;
            btnAddNew.Text = "Cancel";
        }
        else
        {
            dvTransalation.Visible = false;
            btnAddNew.Text = "Add New";
        }
    }

    protected void btnSignOut_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
        {
            HttpCookie myCookie = new HttpCookie(FormsAuthentication.FormsCookieName);
            //myCookie.Expires = DateTime.Now.AddDays(-1d);
            myCookie.Expires = DateTime.Now.AddMinutes(50);

            Response.Cookies.Add(myCookie);
            FormsAuthentication.SignOut();
            //
            Session.Remove("userid");
            Session.Remove("token");
        }
        Response.Redirect("Login.aspx");
    }

}
