﻿using Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CropManagement : System.Web.UI.Page
{
    string sServerId;
    string sDbId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ListOfServers oSrvList = new ListOfServers();
            ddlSrv.DataTextField = "ServerName";
            ddlSrv.DataValueField = "ServerId";
            ddlSrv.DataSource = oSrvList.stages;
            ddlSrv.DataBind();  
        }
    }



    protected void ddlSrv_SelectedIndexChanged(object sender, EventArgs e)
    {
        BusinessLogic bl = new BusinessLogic();

        ddlDBNames.DataTextField = "name";
        ddlDBNames.DataValueField = "database_id";
        ddlDBNames.DataSource = bl.GetDBList();
        ddlDBNames.Visible = true;
        ddlDBNames.DataBind();
        
        ddlDBNames.Focus();
    }
    protected void ddlDBNames_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddl = (DropDownList)sender;
        sDbId = (string)ddl.SelectedItem.Text; 

        //sServerId = (string)ddlSrv.SelectedValue;

        ListOfServers oSrvList2 = new ListOfServers();
       // object o = oSrvList2.Where(i => i.Property == 2).FirstOrDefault();

        var myItem = oSrvList2.stages.Find(item => item.ServerId == 2);

        DataAccess oDataAccess = new DataAccess(sDbId, myItem.DataSource.ToString(), myItem.UserId, myItem.Pass);

    }
}