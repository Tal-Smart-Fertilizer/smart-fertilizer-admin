﻿using Smart.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Smart.Authorisation;
using Smart.DataManager;
using System.Web.Security;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(tbUserName.Text) & !string.IsNullOrEmpty(tbPassword.Text))
        {
            UserCredentials oUserCredentials = new UserCredentials();
            oUserCredentials.UserName = tbUserName.Text;
            oUserCredentials.Password = tbPassword.Text;
            oUserCredentials.UserIp = "127.0.0.2";

            Smart.Entities.LoginStatus loginStatus = Account.Login(oUserCredentials);

            bool bIsAdmin = ManageUser.isAdmin(loginStatus.UserId, loginStatus.Token);
            if (!bIsAdmin)
            {
                bool isLogedOut = ManageUser.logOut(loginStatus.UserId);
            }
            else {

                Session["userid"] = loginStatus.UserId;
                Session["token"] = loginStatus.Token;

                FormsAuthentication.RedirectFromLoginPage(oUserCredentials.UserName, true);                
            }
        }

    }
}