﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="userimport.aspx.cs" Inherits="userimport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:Button ID="btnSignOut" runat="server" Text="Sign Out" OnClick="btnSignOut_Click" />
    &nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnTranslation" runat="server" Text="Label Translation" OnClick="btnTranslation_Click" />

    <link href="Content/StyleSheet.css" rel="stylesheet" />
    <br />
    <br />
    <div>
        <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" OnMenuItemClick="Menu1_MenuItemClick">
            <Items>
                <asp:MenuItem Text="Import" Value="T1"></asp:MenuItem>
                <asp:MenuItem Text="Messages" Value="T2"></asp:MenuItem>
                <asp:MenuItem Text="Emails" Value="T3"></asp:MenuItem>
            </Items>
            <StaticMenuStyle CssClass="tab" />
            <StaticMenuItemStyle CssClass="item" />
            <StaticSelectedStyle CssClass="selectedMenu" />
        </asp:Menu>
    </div>
    <div class="conteudo">
        <asp:MultiView ID="MultiView1" runat="server" ValidateRequestMode="Disabled" ActiveViewIndex="0">
            <asp:View ID="View1" runat="server">
                <h2>Import</h2>
                <p>Please choose appropriate excel file and click upload.</p>
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="Upload_Click" />
                <br />
                <br />

                <asp:GridView ID="gvMapping" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvMapping_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Header 1">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlUploadedFields" runat="server" AppendDataBoundItems="true">
                                    <asp:ListItem Value="-1">Select</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <br />
                <asp:Button ID="btnBulkInsert" runat="server" Text="Bulk insert" Visible="false" OnClick="btnBulkInsert_Click" />
                Default fields:  Language  
                <asp:DropDownList ID="ddlDefaultLanguage" runat="server">
                    <asp:ListItem Value="1" Selected="true">English</asp:ListItem>
                    <asp:ListItem Value="2">Spanish</asp:ListItem>
                </asp:DropDownList>
                <br />

                <br />
                <div class="row">
                    <asp:GridView ID="gvImport" runat="server">
                        <Columns>
                        </Columns>
                    </asp:GridView>
                </div>

                <div id="dresult" runat="server">
                    <h2>Right Results:</h2>
                    <div class="row">
                        <asp:GridView ID="gvRightResult" runat="server">
                            <Columns>
                            </Columns>
                        </asp:GridView>
                    </div>

                    <h2>False Results:</h2>
                    <div class="row">
                        <asp:GridView ID="gvFalseResult" runat="server">
                            <Columns>
                            </Columns>
                        </asp:GridView>
                    </div>

                </div>

            </asp:View>
            <asp:View ID="View2" runat="server">
                <h2>Messages</h2>
                <br />
                <asp:ObjectDataSource ID="odsMessagesList" runat="server"
                    SelectMethod="SelectAllAdminMessage" DeleteMethod="DeleteAdminMessage"
                    TypeName="Facade.BusinessLogic" OnDeleted="odsMessagesList_Deleted">
                    <DeleteParameters>
                        <asp:ControlParameter Name="MessageId" ControlID="gvMessages" PropertyName="DataKeyNames" />
                    </DeleteParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsMessagesItem" runat="server"
                    SelectMethod="SelectAdminMessage" InsertMethod="InsertAdminMessage" UpdateMethod="UpdateAdminMessage"
                    TypeName="Facade.BusinessLogic" OnInserted="odsMessagesItem_Inserted" OnUpdated="odsMessagesItem_Updated">
                    <SelectParameters>
                        <asp:ControlParameter DefaultValue="1" ControlID="gvMessages" Name="MessageId" PropertyName="SelectedValue" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:Parameter Name="subject" Type="String" />
                        <asp:Parameter Name="body" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:ControlParameter Name="MessageId" ControlID="dvMessageItem" PropertyName="DataKeyNames" />
                        <asp:Parameter Name="subject" Type="String" />
                        <asp:Parameter Name="body" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:DetailsView ID="dvMessageItem" AutoGenerateRows="False" DefaultMode="ReadOnly"
                    DataSourceID="odsMessagesItem" runat="server" DataKeyNames="MessageId">
                    <Fields>
                        <asp:BoundField DataField="subject" HeaderText="Message Subject" SortExpression="subject" />
                        <asp:TemplateField HeaderText="Message Body">
                            <ItemTemplate>
                                <asp:Label ID="lbItemBody" runat="server" Text='<%# Eval("body") %>' />
                            </ItemTemplate>
                            <InsertItemTemplate>
                                <asp:TextBox ID="tbInsertBody" runat="server" TextMode="MultiLine" Text='<%# Bind("body") %>'></asp:TextBox>
                            </InsertItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="tbEditBody" runat="server" TextMode="MultiLine" Text='<%# Bind("body") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" ShowInsertButton="True" />
                    </Fields>
                </asp:DetailsView>
                <br />
                <asp:GridView ID="gvMessages" DataSourceID="odsMessagesList" AutoGenerateColumns="false" DataKeyNames="MessageId" runat="server" OnRowDataBound="gvMessages_RowDataBound" OnRowCommand="gvMessages_RowCommand">
                    <Columns>
                        <asp:CommandField ShowSelectButton="true" />
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbDeleteMessage" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this item?');" runat="server">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject" />
                        <asp:BoundField DataField="Body" HeaderText="Body" SortExpression="Body" />
                        <asp:TemplateField HeaderText="Available Emails">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlAvailableEmails" runat="server" AutoPostBack="true"></asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Recipient">
                            <ItemTemplate>
                                <asp:TextBox ID="tbRecipientMail" runat="server"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Test Email">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnSendTest" runat="server" Text="Send test" CommandName="sendtest" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:View>
            <asp:View ID="View3" runat="server">
                <h2>Emails</h2>
                <br />
                <asp:ObjectDataSource ID="odsEmailsDetails" runat="server" InsertMethod="InsertAdminEmail" OnDeleted="odsEmailsDetails_Deleted" OnInserted="odsEmailsDetails_Inserted" OnUpdated="odsEmailsDetails_Updated" SelectMethod="SelectAdminEmail" TypeName="Facade.BusinessLogic" UpdateMethod="UpdateAdminEmail">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="gvEmails" DefaultValue="1" Name="EmailId" PropertyName="SelectedValue" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Address" Type="String" />
                        <asp:Parameter Name="Password" Type="String" />
                        <asp:Parameter Name="Host" Type="String" />
                        <asp:Parameter Name="Port" Type="Int32" />
                        <asp:Parameter Name="Timeout" Type="Int32" />
                        <asp:Parameter Name="EnableSsl" Type="Boolean" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="dvEmails" Name="EmailId" PropertyName="DataKeyNames" />
                        <asp:Parameter Name="Address" Type="String" />
                        <asp:Parameter Name="Password" Type="String" />
                        <asp:Parameter Name="Host" Type="String" />
                        <asp:Parameter Name="Port" Type="Int32" />
                        <asp:Parameter Name="Timeout" Type="Int32" />
                        <asp:Parameter Name="EnableSsl" Type="Boolean" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsEmailsList" runat="server"
                    SelectMethod="SelectAllAdminEmail" DeleteMethod="DeleteAdminEmail"
                    TypeName="Facade.BusinessLogic">
                    <DeleteParameters>
                        <asp:ControlParameter Name="EmailId" ControlID="gvEmails" PropertyName="DataKeyNames" />
                    </DeleteParameters>
                </asp:ObjectDataSource>
                <asp:DetailsView ID="dvEmails" AutoGenerateRows="False" DefaultMode="ReadOnly"
                    DataSourceID="odsEmailsDetails" runat="server" DataKeyNames="EmailId">
                    <Fields>
                        <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                        <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password" />
                        <asp:BoundField DataField="Host" HeaderText="Host" SortExpression="Host" />
                        <asp:BoundField DataField="Port" HeaderText="Port" SortExpression="Port" />
                        <asp:BoundField DataField="Timeout" HeaderText="Timeout" SortExpression="Timeout" />
                        <asp:CheckBoxField DataField="EnableSsl" HeaderText="EnableSsl" SortExpression="EnableSsl" />
                        <asp:CommandField ShowEditButton="True" ShowInsertButton="True" />
                    </Fields>
                </asp:DetailsView>
                <br />
                <br />
                <asp:GridView ID="gvEmails" DataSourceID="odsEmailsList" AutoGenerateColumns="false" DataKeyNames="EmailId" runat="server">
                    <Columns>
                        <asp:CommandField ShowSelectButton="true" />
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbDeleteEmail" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this item?');" runat="server">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                        <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password" />
                        <asp:BoundField DataField="Host" HeaderText="Host" SortExpression="Host" />
                        <asp:BoundField DataField="Port" HeaderText="Port" SortExpression="Port" />
                        <asp:BoundField DataField="Timeout" HeaderText="Timeout" SortExpression="Timeout" />
                        <asp:CheckBoxField DataField="EnableSsl" HeaderText="EnableSsl" SortExpression="EnableSsl" />
                    </Columns>
                </asp:GridView>
            </asp:View>
        </asp:MultiView>
    </div>


</asp:Content>

