﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FileManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        DataAccess Dal = new DataAccess();
        DataTable dt = Dal.SelectOldImageFiles();

        int UserId;
        string FileUploadName;
        string FileUploadDescription;
        int EntityTypeId;
        int EntityId;
        int ParentEntityId;
        int FileExtensionId;
        Guid new_identity;

        string oldPath = @"D:\temp5\images\images\varieties\"; //@"C:\inetpub\CompoClient\assets\images\varieties\"; //
        string newpath = @"D:\temp5\newimages\"; //@"C:\inetpub\FileManagement\compo\varieties"; //

        foreach (DataRow dataRow in dt.Rows)
        {
            try
            {
                UserId = Int32.Parse(dataRow["CreatedByUserId"].ToString());
                FileUploadName = dataRow["ImageName"].ToString();
                FileUploadDescription = dataRow["ImageName"].ToString();
                EntityTypeId = 4;//Int32.Parse(dataRow["Email"].ToString());
                EntityId = Int32.Parse(dataRow["VarietyiId"].ToString());
                ParentEntityId = Int32.Parse(dataRow["CropId"].ToString());
                FileExtensionId = 1; //Int32.Parse(dataRow["Email"].ToString());
                new_identity = Guid.Empty;

                new_identity = Dal.InsertFileUpload(UserId, FileUploadName, FileUploadDescription, EntityTypeId, EntityId, ParentEntityId, FileExtensionId);

                string newFileName = new_identity.ToString();

                FileInfo f1 = new FileInfo(oldPath + FileUploadName);

                if (f1.Exists)
                {
                    //if (!Directory.Exists(newpath))
                    //{
                    //    Directory.CreateDirectory(newpath);
                    //}
                    f1.CopyTo(string.Format("{0}{1}{2}", newpath, newFileName, f1.Extension));
                }
            }
            catch { }
        }
    }
}